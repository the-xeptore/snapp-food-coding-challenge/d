# Snapp Food Coding Challenge - D

Snapp food coding challenge test D solution

## Implementation Details

This version will **only** delegates the squaring stage to workers and each one passes the squared result immediately to its outbound channel.

See [`complete-workers`](/complete-workers) branch for different implementation.

## Running

### Installing prerequisites

- Install Go:

  Download and install latest version of Go compiler depending on you operating system from: <https://golang.org/dl/>.

  Ensure Go is correctly installed and is accessible:

  ```sh
  go version
  ```

  As the time of writing, latest version of Go is `1.16.3`.

- Install git:

  Download and install git depending on ypu operating system directly from [its website](https://git-scm.com/downloads), or using your operating system package manager.

  Ensure git is correctly installed and is accessible:

  ```sh
  git --version
  ```

  As the time of writing, latest version of git is `2.31.1`.

- Install make:

  Most Unix-based operating system should already include make. For Windows see:

  - <https://stackoverflow.com/a/54086635/9109337>
  - <https://stackoverflow.com/a/32127632/9109337>
  - <http://gnuwin32.sourceforge.net/packages/make.htm>
  - <https://stackoverflow.com/a/51560962/9109337>

  Ensure make is correctly installed and is accessible:

  ```sh
  make --version
  ```

  As the time of writing, latest version of make is `4.3`.

### Cloning

Clone the repository:

```sh
git clone git@gitlab.com:the-xeptore/snapp-food-coding-challenge/d.git
```

### Configure

You can set maximum number of squaring stage worker routines by changing the `square_workers` variable value in [config.go](/config.go), which is by default `2`, as mentioned in the problem specification.

### Build

```sh
make build
```

### Run

```sh
make run
```

### Test

```sh
make test
```
