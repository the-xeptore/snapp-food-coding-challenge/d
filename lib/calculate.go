package lib

func CalculateSquaredSum(workers int, nums []int) int64 {
	in := gen(nums)

	sqs := make([]<-chan int, len(nums))
	for i := 0; i < len(nums); i++ {
		sqs[i] = sq(in)
	}

	var sum int64 = 0
	for num := range merge(sqs) {
		sum += int64(num)
	}

	return sum
}
