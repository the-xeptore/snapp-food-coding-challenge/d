package lib

import (
	"testing"
)

func TestCalculateSquaredSum(t *testing.T) {
	nums := []int{12, 54, 89, 21, 66, 47, 14, 285, 96}
	var expected int64 = 108624
	actual := CalculateSquaredSum(4, nums)
	if actual != expected {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", actual, expected)
	}
}
