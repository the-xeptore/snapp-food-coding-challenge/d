.PHONY: build run test

GO = go

build:
	$(GO) build -o app

run: build
	./app

test:
	$(GO) test gitlab.com/the-xeptore/snapp-food-coding-challenge/d/lib
