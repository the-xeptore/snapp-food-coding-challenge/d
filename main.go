package main

import (
	"fmt"

	"gitlab.com/the-xeptore/snapp-food-coding-challenge/d/lib"
)

func main() {
	nums := []int{12, 54, 89, 21, 66, 47, 14, 285, 96}

	sum := lib.CalculateSquaredSum(square_workers, nums)

	fmt.Println("Sum Squared:", sum)
}
